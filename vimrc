" ************************ VUNDLE ************************

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'Raimondi/delimitMate'

call vundle#end()            " required
filetype plugin indent on    " required


" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ

" ************************ END VUNDLE ************************

" show line numbers
set number

" soft tabs, 4 spaces
set expandtab
set tabstop=4 

" use syntax highlighting
syntax on

" display a light grey bar at column 80
set colorcolumn=80
highlight ColorColumn ctermbg=lightgrey guibg=lightgrey 

" auto indentation
set smartindent

" tab width == 8 for go files
autocmd Filetype go setlocal tabstop=8 noexpandtab

" tab width == 8 for c files
autocmd Filetype c setlocal tabstop=8 noexpandtab nosmartindent cindent

autocmd Filetype asm setlocal tabstop=8 noexpandtab nosmartindent cindent

" line width == 80 for markdown and txt files
autocmd Filetype markdown setlocal textwidth=80
autocmd Filetype text setlocal textwidth=80

" ***** MAPPINGS ***** "
"
" SPELLCHECK
:map <C-F> : set spell! spelllang=fr<CR> : echo 'Correcteur français' <CR>
:map <C-E> : set spell! spelllang=en_us<CR> : echo 'Spellcheck in English' <CR>
:map <C-P> : set spell! spelllang=pt_br<CR> : echo 'Corretor ortográfico Português' <CR>
